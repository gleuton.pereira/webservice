<?php
/**
 * User: gleuton
 * Date: 09/01/18
 * Time: 21:54
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'name',
        'email',
        'phone'
    ];

    public function address()
    {
        return $this->hasMany(Address::class);
    }
}