<?php
/**
 * User: gleuton
 * Date: 09/01/18
 * Time: 21:50
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'address',
        'city',
        'state',
        'zipcode'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}