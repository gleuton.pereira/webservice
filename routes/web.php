<?php


    $router->get('/', function () use ($router) {
        return 'home: "/" <br>' . $router->app->version();
    });

    $router->group(
        [
            'prefix' => 'api/clients'
        ],
        function () use ($router) {
            $router->get('', 'ClientsController@index');
            $router->get('{id}', 'ClientsController@show');
            $router->post('', 'ClientsController@store');
            $router->put('{id}', 'ClientsController@update');
            $router->delete('{id}', 'ClientsController@destroy');
        }
    );

    $router->group(
        [
            'prefix' => 'api/clients/{clienteId}/addresses'
        ],
        function () use ($router) {
            $router->get('', 'AddressesController@index');
            $router->get('{id}', 'AddressesController@show');
            $router->post('', 'AddressesController@store');
            $router->put('{id}', 'AddressesController@update');
            $router->delete('{id}', 'AddressesController@destroy');
        }
    );

    $router->get('TCU', function () {
        $client = new \Zend\Soap\Client('http://contas.tcu.gov.br/debito/CalculoDebito?wsdl');
        echo "Informações do Servidor: ";
        print_r($client->getOptions());
        echo "Funções: ";
        print_r($client->getFunctions());
        echo "Tipos: ";
        print_r($client->getTypes());
        echo "Resultado: ";


        print_r($client->obterSaldoAtualizado([
            'parcelas' => [
                'parcela' => [
                    'data' => '1992-02-04',
                    'tipo' => 'D',
                    'valor' => '18'
                ]

            ],
            'aplicaJuros' => true,
            'dataAtualizacao' => '2018-01-20'
        ]));


    });
    $uri = 'http://webservice.local';
    $wsdl = 'webservice.wsdl';

    $router->get($wsdl, function () use ($uri) {
        $autoDiscover = new \Zend\Soap\AutoDiscover();
        $autoDiscover->setUri($uri . '/server');
        $autoDiscover->setServiceName('SONSOAP');
        $autoDiscover->addFunction('soma');
        $autoDiscover->handle();
    });

    $router->post('server', function () use ($uri, $wsdl) {
        $server = new \Zend\Soap\Server($uri . '/' . $wsdl, [
            'cache_wsdl' => WSDL_CACHE_NONE
        ]);

        $server->setUri($uri . '/server');

        return $server->setReturnResponse(true)
            ->addFunction('soma')
            ->handle();
    });

    $router->get('soap-test', function () use ($uri, $wsdl) {
        $client = new \Zend\Soap\Client($uri . '/' . $wsdl, [
            'cache_wsdl' => WSDL_CACHE_NONE
        ]);
        print_r($client->soma(100, 200));
    });


    // soap server com client
    $router->get('client/' . $wsdl, function () use ($uri) {
        $autoDiscover = new \Zend\Soap\AutoDiscover();
        $autoDiscover->setUri($uri . '/client/server');
        $autoDiscover->setServiceName('SONSOAP');
        $autoDiscover->setClass(\App\Http\Controllers\Soap\ClientsSoap::class);
        $autoDiscover->handle();
    });

    $router->post('client/server', function () use ($uri, $wsdl) {
        $server = new \Zend\Soap\Server($uri . '/client/' . $wsdl, [
            'cache_wsdl' => WSDL_CACHE_NONE
        ]);

        $server->setUri($uri . '/client/server');

        return $server->setReturnResponse(true)
            ->setClass(\App\Http\Controllers\Soap\ClientsSoap::class)
            ->handle();
    });

    $router->get('soap-client', function () use ($uri, $wsdl) {
        $client = new \Zend\Soap\Client($uri . '/client/' . $wsdl, [
            'cache_wsdl' => WSDL_CACHE_NONE
        ]);

        print_r($client->create([
            'name' => 'Luiz Carlos',
            'email' => 'luizcarlos@schoolofnet.com',
            'phone' => '5555',
        ]));
        //print_r($client->getAll());
    });

    /**
     * @param int $num1
     * @param int $num2
     * @return int
     */
    function soma($num1, $num2)
    {
        return $num1 + $num2;
    }

